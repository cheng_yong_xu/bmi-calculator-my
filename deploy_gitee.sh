#!/usr/bin/env sh
 
# 确保脚本抛出遇到的错误
set -e

# 检查参数个数
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <your_branch> <commit-message>"
    exit 1
fi

# 获取参数
YOUR_BRANCH=$1
COMMIT_MESSAGE=$2

# 检查是否在Git仓库中
if [ ! -d ".git" ]; then
    echo "Error: 当前文件不是一个Git仓库"
    exit 1
fi

# 切换到主分支
git checkout master

# 根据你输入的参数创建分支
git checkout -b "$YOUR_BRANCH"

# 添加所有更改到暂存区
git add .

# 提交更改
git commit -m "$COMMIT_MESSAGE"

# 检查git commit是否成功执行
if [ "$?" -ne 0 ]; then
    echo "提交失败！！！请检查您的更改"
    exit 1
fi

# 推送到远程仓库分支
git push -u origin "$YOUR_BRANCH"

# 检查git push是否成功执行
if [ "$?" -eq 0 ]; then
    echo "更改已成功提交并推送到远程存储库√√√"
else
    echo "推送到远程存储库失败！！！"
    exit 1
fi

# 切换到主分支
git checkout master

# 合并分支
git merge "$YOUR_BRANCH"

# 推送到远程仓库分支
git push -u origin "$YOUR_BRANCH"

# 推送到远程仓库主分支
git push

# 检查git push是否成功执行
if [ "$?" -eq 0 ]; then
    echo "更改已成功提交并推送到远程存储库√√√"
else
    echo "推送到远程存储库失败！！！"
    exit 1
fi