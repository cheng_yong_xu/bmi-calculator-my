import React from 'react'
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types'

const Bar = ({ labelData, bmiData }) => {

  // 定义图标数据
  const data = canvas => {
    // 从传入的canvas元素中获取2D绘图上下文，这是在canvas上绘制图形的基础。
    // 这段代码创建了一个线性渐变对象，起始于坐标(63, 81)，结束于(181, 700)，颜色从#929dd9渐变到#172b4d。这常用于为图表的填充色提供动态效果。
    const ctx = canvas.getContext("2d");
    const gradient = ctx.createLinearGradient(63, 81, 181, 700);
    gradient.addColorStop(0, '#929dd9');
    gradient.addColorStop(1, '#172b4d');
    return {
      labels: labelData,  // 图表的标签数组，通常对应X轴的各个分类
      datasets: [  // 一个数据集对象
        {
          label: 'BMI',  // 数据集的标签，通常用于图例
          data: bmiData,  // 数据集的实际数值数组，对应Y轴的值。
          backgroundColor: gradient,  // 使用之前创建的gradient作为填充色。
          borderColor: '#3F51B5',  // 数据点的边框颜色为#3F51B5。
          pointRadius: 6,  // 数据点的半径为6。
          pointHoverRadius: 8,  // 鼠标悬停时数据点的半径增大到8。
          pointHoverBorderColor: 'white',  // 鼠标悬停时数据点边框颜色变为白色。
          pointHoverBorderWidth: 2  // 鼠标悬停时数据点边框宽度为2。
        }
      ]
    }
  }

  // options 该对象包含了配置信息，主要用来定制基于Chart.js库的图表外观和行为
  const options = {
    responsive: true, // 设置图表是否应响应式
    scales: { //定义图表的坐标轴配置，包括x轴（xAxes）和y轴（yAxes）的样式和行为
      xAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: 'Date',
            fontSize: 18,
            fontColor: 'white'
          },
          gridLines: {
            display: false,
            color: 'white'
          },
          ticks: {
            fontColor: 'white',
            fontSize: 16
          }
        }
      ],
      yAxes: [
        {
          scaleLabel: { //  x轴标题的配置。
            display: true,  // 是否显示x轴标题
            labelString: 'BMI',  // x轴标题的文本内容
            fontSize: 18,  // 标题的字体大小和颜色
            fontColor: 'white'
          },
          gridLines: {  // 网格线的配置
            display: false,  // 不显示x轴的网格线
            color: 'white'  // 格线的颜色，即使不显示也定义了颜色
          },
          ticks: {  // 刻度线的配置
            fontColor: 'white', // 刻度线标签的字体颜色和大小。
            fontSize: 16,
            beginAtZero: true   // 图表的y轴刻度从0开始
          }
        }
      ]
    },
    tooltips: { // 定义图表提示框（tooltip）的样式。
      // 分别设置提示框标题和内容的字体大小。
      titleFontSize: 13,
      bodyFontSize: 13
    }
  }
  return (
    <>
      <Line data={data} options={options} />
    </>
  )
}

Bar.propTypes = {
  labelData: PropTypes.array,
  bmiData: PropTypes.array
}

export default Bar