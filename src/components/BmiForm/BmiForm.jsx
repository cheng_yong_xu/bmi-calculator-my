import React, { useState } from 'react';
import PropTypes from 'prop-types';
import '../App/App.css'

const Input_initialValues = {
	weight: '',
	height: '',
	date: ''
}

const BmiForm = ({ Input_handleChange }) => {
	// 定义，初始化数据状态
	const [Input_state, setState_Input] = useState(Input_initialValues)

	// input 改变时，更新数据
	const handleChange = e => {
		let { value, name } = e.target;
		// 输入的数字不能大于999
		if (value > 999) {
			value = 999
		}
		const date = new Date().toLocaleString().split(',')[0]
		// console.log(date)
		// 更新输入框的值
		setState_Input({
			...Input_state,
			[name]: value,
			date
		})
	}

	  // 提交数据
		const handleSubmit = () => {
			Input_handleChange(Input_state)
			setState_Input(Input_initialValues)
			// console.log('已提交', Input_state)
			// console.log('已提交', Input_initialValues)
		}

	return (
		<>
			{/* 输入框 */}
					<div className='row'>
						<div className='col m6 s12'>
							<label htmlFor="weight">Weight (in kg)</label>
							<input
								type="number"
								id="weight"
								name="weight"
								min="1"
								max="999"
								placeholder="50"
								value={Input_state.weight}
								onChange={handleChange}
							/>
						</div>

						<div className='col m6 s12'>
							<label htmlFor="height">Height (in cm)</label>
							<input
								type="number"
								id="height"
								name="height"
								min="1"
								max="999"
								placeholder="175"
								value={Input_state.height}
								onChange={handleChange}
							/>
						</div>
					</div>

					<div className='center'>
						<button
							id="bmi-btn"
							className="calculate-btn"
							type="button"
							disabled={!Input_state.weight || !Input_state.height}
							onClick={handleSubmit}
						>Calculate BMI</button>
					</div>
		</>
	)
}

BmiForm.propTypes ={
	Input_handleChange: PropTypes.func.isRequired
}

export default BmiForm;
